FROM groovy:jdk8
USER root
# Create app directory
RUN mkdir -p /app
WORKDIR /app

COPY build/libs/buratino.jar /app/app.jar

EXPOSE 8080

CMD [ "java", "-jar", "app.jar" ]

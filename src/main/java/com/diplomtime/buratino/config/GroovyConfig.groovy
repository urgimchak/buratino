package com.diplomtime.buratino.config


import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy

import javax.sql.DataSource

@Configuration
class GroovyConfig {
    @Autowired
    DataSourceProperties dataSourceProperties

    @Bean
    Sql sql() {
        return new Sql(dataSourceProxy())
    }

    @Bean
    DataSource dataSourceProxy() {
        return new TransactionAwareDataSourceProxy(dataSourceProperties.initializeDataSourceBuilder().build())
    }
}


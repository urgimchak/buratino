package com.diplomtime.buratino.sql

import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DBExecutor {

    @Autowired
    Sql sql

    def InsertPayment(def values){

        def orderNumber = numberOfOrder(values)
        def query = """
                        INSERT INTO PostupleniaFinansov 
                            (ZakazId,     Summ_1,        DateOplati_1,  Kassir_1,DateTransaction_1,Check_1) 
                     VALUES ($orderNumber,$values.Amount,current_timestamp,  1001908, current_timestamp,     36)
                    """


        sql.execute(query)

        //если заказ в продажах, то переводим в производство
        if(IsOrderInSailings(values))
        {
            InsertHistoryOfTransferStadia(values)
            query = """
                        UPDATE Заказы SET 
                         Data_priema = current_timestamp,
                         MicroStad = 1300,
                         PredvaritZakaz = 0
                         where [Key] = $orderNumber         
                    """
            sql.execute(query)

            //записываем о трнасфере заказа

        }


    }

    def UpdatePayment(def values)
    {
        def orderNumber = numberOfOrder(values)
        String paymentNumber = paymentCount(values)
        def query = " UPDATE PostupleniaFinansov SET "+
                            "DateOplati_"+ paymentNumber +"= current_timestamp, "+
                            "Summ_"+ paymentNumber +"= $values.Amount, "+
                            "Kassir_"+ paymentNumber +"= 1001908, "+
                            "DateTransaction_"+ paymentNumber +"= current_timestamp, " +
                            "Check_"+ paymentNumber +" = 36 "+
                            "where ZakazId = $orderNumber"
        sql.execute(query)

        //если заказ в продажах
        if(IsOrderInSailings(values))
        {
            //записываем о трнасфере заказа
            InsertHistoryOfTransferStadia(values)
            query = """
                        UPDATE Заказы SET 
                         Data_priema = current_timestamp,
                         MicroStad = 1300,
                         PredvaritZakaz = 0
                         where [Key] = $orderNumber         
                    """
            sql.execute(query)


        }
    }

    boolean IsPaymentExist(def values)
    {
        def orderId = ParseKodPosrednika(values)
        def query = """
                        SELECT * FROM Заказы
                        INNER JOIN PostupleniaFinansov as PF ON Заказы.[Key]=PF.ZakazId
                        WHERE Заказы.Kod_posrednika = '$orderId' 
                    """
        def row=sql.rows(query)
    }

    //возвращает номер колонки с первым незаполненным платежом
    String paymentCount(def values)
    {

        def orderId = ParseKodPosrednika(values)
        def query = """
                       SELECT
                       case
                       when (Summ_1 is NULL or Summ_1 = '0') then '1'
                       when (Summ_2 is NULL or Summ_2 = '0') then '2'
                       when (Summ_3 is NULL or Summ_3 = '0') then '3'
                       when (Summ_4 is NULL or Summ_4 = '0') then '4'
                       when (Summ_5 is NULL or Summ_5 = '0') then '5'
                       when (Summ_6 is NULL or Summ_6 = '0') then '6'
                       when (Summ_7 is NULL or Summ_7 = '0') then '7'
                       when (Summ_8 is NULL or Summ_8 = '0') then '8'
                       when (Summ_9 is NULL or Summ_9 = '0') then '9'
                       when (Summ_10 is NULL or Summ_10 = '0') then '10'
                        else '10'
                         end as nextPayment
                       FROM Заказы
                       inner join PostupleniaFinansov on ZakazId=[Key]
                       Where Kod_posrednika='$orderId'
                    """
        return sql.firstRow(query).nextPayment
    }

    //возвращает код заказа
    int numberOfOrder(def values)
    {

        def orderId = ParseKodPosrednika(values)
        def query = """
                       SELECT * FROM Заказы
                       Where Kod_posrednika='$orderId'
                    """
        return sql.firstRow(query).Key
    }

    def InsertHistoryOfTransferStadia(def values)
    {
        def lastStadia = LastStadia(values)
        def orderNumber = numberOfOrder(values)
        def query= """
                INSERT INTO OrderHistoryOfTranserStadia (OrderID,SotrudnikID,LastStadiaID,NewStadidID,TransferData,Motiv)
                VALUES                                  ($orderNumber,1001908,$lastStadia,1300,current_timestamp,'Авто перевод')
        """
        sql.execute(query)
    }

    def IsOrderInSailings(def values)
    {
        def orderId = ParseKodPosrednika(values)
        def query ="""
                            SELECT * FROM Заказы
                            WHERE Заказы.Kod_posrednika = '$orderId'
                   """
        def row = sql.firstRow(query)
        return (row.Microstad < 1300 && row.PredvaritZakaz)
    }

    def LastStadia(def values)
    {
        def orderId = ParseKodPosrednika(values)
        def query ="""
                            SELECT * FROM Заказы
                            WHERE Заказы.Kod_posrednika = '$orderId'
                   """
        return sql.firstRow(query).Microstad
    }

    def IsOrderInProduction(def values)
    {
        def orderId = ParseKodPosrednika(values)
        def query ="""
                            SELECT * FROM Заказы
                            WHERE Заказы.Kod_posrednika = '$orderId'
                   """
        def row = sql.firstRow(query)
        return (row.Microstad >= 1300 && row.Microstad <=3150 && !row.PredvaritZakaz)

    }

    static ParseKodPosrednika(def values)
    {
        def delimeter = "-"
        def subStr = values.OrderId.split(delimeter)
        return subStr[0]
    }

    def clientInfo(values)
    {
        def orderId = ParseKodPosrednika(values)
        def query = """
            Select Email,PhoneMobile,KlName from Заказы
            inner join Klient K ON Заказы.KlientID = K.KlientID
            where Kod_posrednika = '$orderId'
        """
        def clientInfo = sql.firstRow(query)
        return clientInfo
    }

    def IsOrderExists(values)
    {
        def orderId = ParseKodPosrednika(values)
        def query = """
                     SELECT * FROM Заказы
                     Where Kod_posrednika='$orderId'
                    """
        return sql.firstRow(query)
    }
}

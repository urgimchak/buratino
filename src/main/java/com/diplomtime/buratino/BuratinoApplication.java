package com.diplomtime.buratino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuratinoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuratinoApplication.class, args);
	}
}

package com.diplomtime.buratino.rabbit


class NotificationAboutPayment {
    private String type
    private String message
    private String title
    private String toAddress
    private String fromAddress

    NotificationAboutPayment()
    {
        type = null
        message = ""
        toAddress = ""
        fromAddress = ""
        title = ""
    }

    String getType() {
        return type
    }

    String getMessage() {
        return message
    }

    String getTitle() {
        return title
    }

    String getToAddress() {
        return toAddress
    }

    String getFromAddress() {
        return fromAddress
    }

    void setType(String type) {
        this.type = type
    }

    void setMessage(String message) {
        this.message = message
    }

    void setTitle(String title) {
        this.title = title
    }

    void setToAddress(String toAddress) {
        this.toAddress = toAddress
    }

    void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress
    }
}

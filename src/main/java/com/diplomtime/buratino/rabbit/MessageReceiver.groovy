package com.diplomtime.buratino.rabbit

import com.diplomtime.buratino.sql.DBExecutor
import groovy.json.JsonSlurper
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

import java.time.LocalDateTime

@Component
class MessageReceiver {

    @Autowired
    DBExecutor dbExecutor

    @Autowired
    MessageProducer messageProducer

    @Transactional
    @RabbitListener(queues='${buratino.queue.tinkoff}')
    void receiveMessage(Message message)
    {

        System.out.println("Received -> " + new String(message.getBody()) + ">")

        try {
            def values = new JsonSlurper().parseText(new String(message.getBody()))
            if (values.Success == "true" &&
                                    (dbExecutor.IsOrderInSailings(values) || dbExecutor.IsOrderInProduction(values))) {

                if (dbExecutor.IsPaymentExist(values)) {
                    dbExecutor.UpdatePayment(values)
                    messageProducer.sendMsgAboutTransaction(values,LocalDateTime.now().toString())

                } else {
                    dbExecutor.InsertPayment(values)
                    messageProducer.sendMsgAboutTransaction(values,LocalDateTime.now().toString())
                }
            } else {
                //если заказ не в продажах и не в производстве, то отправляем мессадж в отстойник
                System.out.println("Какие-то левые данные")
                messageProducer.produceMsgToStall(message)

            }
        }catch (Exception e)
        {
            //если какая-то ошибка, то отправляем мэссадж в отстойник
            System.out.println(e.toString()+"\nMessage retranslated to stall")
            messageProducer.produceMsgToStall(message)
        }
    }
}
package com.diplomtime.buratino.rabbit

import com.diplomtime.buratino.sql.DBExecutor
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import java.time.LocalDateTime

@Component
class MessageProducer {

    @Autowired
    private AmqpTemplate amqpTemplate

    @Autowired
    private RabbitTemplate rabbitTemplate

    @Value('${buratino.queue.tinkoff.stall}')
    private String stallQueueName

    @Value('${buratino.queue.margaritka}')
    private String margaritkaQueueName

    @Autowired
    private DBExecutor dbExecutor

    void produceMsgToStall(Message msg){
        amqpTemplate.convertAndSend(stallQueueName, msg)
    }

    void sendMsgAboutTransaction(def values,String dateTime){

        def clientInfo=dbExecutor.clientInfo(values)
        def msg = new NotificationAboutPayment()
        msg.setType("SENDGRID")
        msg.setFromAddress("info@diplomtime.ru")
        msg.setTitle("Поуступил платёж по заказу №"+dbExecutor.numberOfOrder(values))
        msg.setToAddress("project+16318@i-consulting.planfix.ru")
        msg.setMessage("Код посредника: $values.OrderId \n" +
                       "Дата платежа: $dateTime\n" +
                       "Cумма платежа $values.Amount\n" +
                       "Источник: Эквайринг Тинькофф\n" +
                       "Клиент: $clientInfo")
        rabbitTemplate.convertAndSend(margaritkaQueueName, msg)

    }
}

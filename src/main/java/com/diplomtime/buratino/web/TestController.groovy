package com.diplomtime.buratino.web

import com.diplomtime.buratino.rabbit.MessageProducer
import com.diplomtime.buratino.sql.DBExecutor
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController("myTestController")
class TestController {

    @Autowired
    Sql sql

    @Autowired
    MessageProducer messageProducer

    @Autowired
    DBExecutor dbPayments

    @Transactional
    @GetMapping(path= "/test")
    List<Object> myTestFunc()
    {

        return dbPayments.IsOrderInSailings()
    }
}
